const { body } = require('express-validator')

const createCategoryRules = [
    body('name').notEmpty().withMessage('name is required')
]

const createProductRules = [
    body('name').notEmpty().withMessage('name is required')
]

const registerRules = [
    body('email').isEmail().withMessage('email invalid').notEmpty().withMessage('email is required'),
    body('name').notEmpty().withMessage('name is required'),
    body('password').notEmpty().withMessage('password is required')
]

module.exports = {
    createCategoryRules,
    createProductRules,
    registerRules
}