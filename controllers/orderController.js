const model = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const datas = await model.order.findAll({
                include: {
                    model: model.offer,
                    required: true,
                    include: {
                        model: model.product,
                        required: true
                    }
                },
                where: {
                    '$offer.product.userId$': res.locals.user.id
                }
            });

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "Data order successfully listed",
                "data" : datas
            })
        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            })
        }
    },

    create: async (req, res) => {
        try {
          const data = await model.order.create({ ...req.body, status: 'accepted'});
    
          return res.status(200).json({
            "success" : true,
            "error" : 0,
            "message" : "Data order successfully created",
            "data" : data,
          });
        } catch (error) {
          return res.status(500).json({
            "success" : false,
            "error" : error.code,
            "message" : error,
            "data" : null,
          });
        }
    },

    update: async (req, res) => {
        try {
            const data = await model.order.update(
            {
                name: req.body.name,
                description: req.body.description,
                storageId: req.body.storageId,
            },
            {
                where: {
                id: req.body.id,
                },
            }
            );

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "Data order successfully updated",
                "data" : data,
            });
        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null,
            });
        }
    },

    getById: async (req, res) => {
        try {
            const data = await model.order.findOne({
                where: {
                    offerId: req.params.id
                }
            })

            if (!data){
                return res.status(404).json({
                    "success" : false,
                    "error" : 0,
                    "message" : "Order not found",
                    "data" : null,
                })
            }

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "Order successfully listed",
                "data" : data,
            })
    
        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null,
            })
        }
    },

    destroy: async (req, res) => {
        try {
            const data = await model.order.destroy({
            where: {
                id: req.body.id,
            },
            });

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "Data order successfully deleted",
                "data" : data,
            });
        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null,
            });
        }
    },        
}