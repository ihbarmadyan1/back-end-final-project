const model = require('../models'),
    { genSalt, hash, compareSync } = require('bcrypt')
    jwt = require('jsonwebtoken') 

const cryptPassword = async (password) => {
    const salt = await genSalt(15)
    
    return hash(password, salt)
}

module.exports = {
    register: async (req, res) => {
        try {
            const data = await model.user.create({
                ...req.body,
                password: await cryptPassword(req.body.password)
            })

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "User successfully registered",
                "data" : data
            })

        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            })
        }
    },

    login: async (req, res) => {
        try {
            
            const userExists = await model.user.findOne({
                where: {
                    email : req.body.email
                }
            })

            if(!userExists)
                return res.status(404).json({
                    "success" : false,
                    "error" : 404,
                    "message" : "User not found",
                    "data" : null
                })
            
            if (compareSync(req.body.password, userExists.password)) {
                const token = jwt.sign(
                    { id: userExists.id, name: userExists.name, email: userExists.email },
                    'password!23',
                    { expiresIn: '24h' }
                )
            
                return res.status(200).json({
                    "success" : true,
                    "error" : 0,
                    "message" : "User successfully login",
                    "data" : {
                        "token" : token
                    }
                })
            }
        
            return res.status(409).json({
                "success" : false,
                "error" : 409,
                "message" : "Invalid Credentials",
                "data" : null
            })
        } catch (error) {
            console.log(error)
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            })
        }
    },

    currentUser: async (req, res) => {
        try {
          const user = await model.user.findByPk(res.locals.user.id, {
            attributes: {
              exclude: [
                'id', 'email', 'password', 'createdAt', 'updatedAt'
              ]
            }
          });
    
          if (!user) {
            return res.status(404).json({
              "success" : false,
              "error" : 404,
              "message" : "User data not found",
            });
          }
    
          return res.status(200).json({
            "success" : true,
            "error" : 0,
            "message" : "User successfully get",
            "data": user,
          });
    
        } catch (error) {
          return res.status(500).json({
            "success" : false,
            "error" : error.code,
            "message" : error,
            "data" : null,
          });
        }
      },

      userById: async (req, res) => {
        try {
          const user = await model.user.findByPk(req.body.id, {
            attributes: {
              exclude: [
                'id', 'email', 'password', 'createdAt', 'updatedAt'
              ]
            }
          });
    
          if (!user) {
            return res.status(404).json({
              "success" : false,
              "error" : 404,
              "message" : "User data not found",
              "data" : null
            })
          }
    
          return res.status(200).json({
            "success" : true,
            "error" : 0,
            "message" : "User successfully get",
            "data" : user,
          })
    
        } catch (error) {
          return res.status(500).json({
            "success" : false,
            "error" : error.code,
            "message" : error,
            "data" : null,
          });
        }
      },    

    details: async (req, res) => {
        try {
          const user = await model.user.findByPk(req.body.id, {
            attributes: {
              exclude: [
                'id', 'email', 'password', 'createdAt', 'updatedAt'
              ]
            }
          });
          
          if (!user) {
            return res.status(500).json({
              "success" : false,
              "error" : 404,
              "message" : "User data not found",
            });
          }
    
          const data = await model.user.update(
            {
              name: req.body.name,
              city: req.body.city,
              address: req.body.address,
              phone: req.body.phone,
              image: req.body.image
            },
            {
              where: {
                id: res.locals.user.id,
              },
            }
          );
    
          return res.status(200).json({
            "success" : true,
            "error" : 0,
            "message" : "Data successfully updated",
            "data" : data,
          });
    
        } catch (error) {
          return res.status(500).json({
            "success" : false,
            "error" : error.code,
            "message" : error,
            "data" : null,
          });
        }
      }

}