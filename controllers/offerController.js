const model = require('../models');
const { Op } = require("sequelize");

async function list(req, res){
    try {
        const datas = await model.offer.findAll({
            include: {
                model: model.product,
                required: true,
            },
            where: {
                '$product.userId$': res.locals.user.id
            }
        });

        return res.status(200).json({
            "success" : true,
            "error" : 0,
            "message" : "Offer successfully listed",
            "data" : datas,
        })

    } catch (error) {
        return res.status(500).json({
            "success" : false,
            "error" : error.code,
            "message" : error,
            "data" : null,
        });
    }
}

async function getById(req, res){
    try {
        const offer = await model.offer.findOne({
            where: {
                userId: res.locals.user.id,
                productId: req.params.id
            }
        });

        return res.status(200).json({
            "success" : true,
            "error" : 0,
            "message" : "Offer successfully listed",
            "data" : offer,
        })

    } catch (error) {
        return res.status(500).json({
            "success" : false,
            "error" : error.code,
            "message" : error,
            "data" : null,
        });
    }
}

async function updateOrCreate(req, res){
    try {
        let offerData = await model.offer.update(
            {
                ...req.body, 
                status: 'pending'
            },
            {
                where: {
                    userId: res.locals.user.id,
                    productId: req.body.productId
                }
            });
            
            if (!offerData[0]){
                offerData = await model.offer.create({ 
                    ...req.body, 
                    userId: res.locals.user.id,
                    status: 'pending'
                });

                return res.status(201).json({
                    "success" : true,
                    "error" : 0,
                    "message" : "Offer successfully created",
                    "data" : offerData,
                })
            }
    
    return res.status(200).json({
        "success" : true,
        "error" : 0,
        "message" : "Offer successfully updated",
        "data" : offerData,
    })

    } catch (error) {
        return res.status(500).json({
            "success" : false,
            "error" : error.code,
            "message" : error,
            "data" : null,
        });
    }
}

async function update(req, res){
    try {
        const offer = await model.offer.update(
        {
            status: req.body.status
        },
        {
            where: {
                id: req.body.id
            },
            returning: true,
            limit: 1
        });

        if (!offer[0]){
            return res.status(404).json({
                "success" : false,
                "error" : 0,
                "message" : "Offer not found",
                "data" : null,
            })
        }

        setOtherOffer(offer[1][0].dataValues)

        return res.status(200).json({
            "success" : true,
            "error" : 0,
            "message" : "Offer successfully updated",
            "data" : offer,
        })

    } catch (error) {
        return res.status(500).json({
            "success" : false,
            "error" : error.code,
            "message" : error,
            "data" : null,
        });
    }
}

async function setOtherOffer({ status, id, productId} ) {
    if (status == 'accepted') {
        const offers = await model.offer.update(
            {
                status: 'rejected'
            }, 
            {
                where: {
                    id: {
                        [Op.not]: [id]
                    },
                    productId: productId
                }
            })
    } else if (status == 'rejected') {
        const offers = await model.offer.update(
            {
                status: 'pending'
            },
            {
                where: {
                    id: {
                        [Op.not]: [id]
                    },
                    productId: productId
                }
            }
        )
    }
}

async function remove(id){
    try {
        const offerById = await model.get(id);
        if(!offerById[0]){
            return {message : `offer with id ${id} not found`}
        }
        const offerDelete = await offerDelete.remove(id);
        return {message : `offer with id ${id} hasbeen deleted`}
    } catch (error) {
        return {message : error}
    }
}

module.exports ={
    list,
    getById,
    updateOrCreate,
    update,
    remove
}