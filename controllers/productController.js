const model = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const datas = await model.product.findAll()

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "Data product successfully listed",
                "data" : datas
            })
        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            })
        }
    },

    saleList: async (req, res) => {
        try {
          const datas = await model.product.findAll({
            where: {
              userId: res.locals.user.id
            }
          });
    
          return res.status(200).json({
            "success" : true,
            "error" : 0,
            "message" : "Data product successfully listed",
            "data" : datas,
          });
    
        } catch (error) {
          return res.status(500).json({
            "success" : false,
            "error" : error.code,
            "message" : error,
            "data" : null,
          });
        }
      },
    
      details: async (req, res) => {
        try {
          const data = await model.product.findOne({
            where: {
              id: req.params.id
            }
          })
    
          return res.status(200).json({
            "success" : true,
            "error" : 0,
            "message" : "Details product successfully listed",
            "data" : data,
          });
    
        } catch (error) {
          return res.status(500).json({
            "success" : false,
            "error" : error.code,
            "message" : error,
            "data" : null,
          });
        }
      },
    
    create: async (req, res) => {
        try {
          const data = await model.product.create({ ...req.body, userId: res.locals.user.id });
    
          return res.status(200).json({
            "success" : true,
            "error" : 0,
            "message" : "Data product successfully created",
            "data" : data,
          });
        } catch (error) {
          return res.status(500).json({
            "success" : false,
            "error" : error.code,
            "message" : error,
            "data" : null,
          });
        }
      },
      
    update: async (req, res) => {
        try {
            const data = await model.product.update(
                {
                    name: req.body.name,
                    price: req.body.price,
                    description: req.body.description,
                    image: req.body.image,
                    categoryId: req.body.categoryId
                },
                {
                    where: {
                    id: req.body.id,
                    },
                }
            );

            if (!data[0]) {
                return res.status(404).json({
                  "success": false,
                  "error" : 0,
                  "message" : "Product not found",
                  "data" : null,
                });
            }
        
            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "Data product successfully updated",
                "data" : data,
            });
        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error": error.code,
                "message" : error,
                "data" : null,
            });
        }
    },

    destroy: async (req, res) => {
        try {
            const data = await model.product.destroy({
            where: {
                id: req.body.id,
            },
            });

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "Data product successfully deleted",
                "data" : data,
            });
        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null,
            });
        }
    },        
}