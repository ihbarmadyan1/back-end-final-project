'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('categories', [
      { 
        name: 'Hobi',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      { 
        name: 'Kendaraan',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      { 
        name: 'Baju',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      { 
        name: 'Elektronik',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      { 
        name: 'Kesehatan',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('categories', null, {});
  }
};