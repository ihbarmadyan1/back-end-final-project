'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class offer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      offer.belongsTo(models.product, {
        foreignKey: 'productId',
      });
      offer.belongsTo(models.user, {
        foreignKey: 'userId'
      });
      offer.hasOne(models.order, {
        foreignKey: 'offerId'
      });
    }
  }
  offer.init({
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    productId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    price: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    sequelize,
    modelName: 'offer',
    tableName: 'offers',
    timestamps: true
  });
  return offer;
};