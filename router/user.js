const express = require('express') 
const router = express.Router() 
const { register, login, currentUser, userById , details } = require('../controllers/userController.js') 
const validate = require('../middleware/validate')
const { registerRules } = require('../validators/rule')
const checkToken = require('../middleware/checkToken')

router.post('/register', validate(registerRules), register)
router.post('/login', login)
router.get('/current-user', checkToken, currentUser);
router.post('/byId', userById);
router.post('/details', checkToken, details);

module.exports = router