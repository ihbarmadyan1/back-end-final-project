const express = require('express')
const router = express.Router()
const categoryRouter = require('./category')
const productRouter = require('./product')
const userRouter = require('./user')
const offerRouter = require('./offer')
const orderRouter = require('./order')

router.get('/', (req, res) => res.send('Application is running'));
router.use('/category', categoryRouter)
router.use('/product', productRouter)
router.use('/user', userRouter)
router.use('/offer', offerRouter)
router.use('/order', orderRouter)

module.exports = router 