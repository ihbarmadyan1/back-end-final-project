const express = require('express') 
const router = express.Router()
const { list, create, getById, update, destroy } = require('../controllers/orderController')
const checkToken = require('../middleware/checkToken')

router.get('/list', checkToken, list)
router.post('/create', checkToken, create)
router.get('/:id', getById)
// router.put('/update', checkToken, update) 
// router.delete('/destroy', checkToken, destroy)

module.exports = router