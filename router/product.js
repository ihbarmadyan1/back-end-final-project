const express = require('express') 
const router = express.Router()
const { list, saleList, details, create, update, destroy } = require('../controllers/productController')
const validate = require('../middleware/validate')
const { createProductRules } = require('../validators/rule')
const checkToken = require('../middleware/checkToken')

router.get('/list', checkToken, list)
router.get('/details/:id', details);
router.get('/sale-list', checkToken, saleList);
router.post('/create', checkToken, validate(createProductRules), create)
router.put('/update', checkToken, update) 
router.delete('/destroy', checkToken, destroy)

module.exports = router