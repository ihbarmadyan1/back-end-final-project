const express = require('express');
const router = express.Router();
const checkToken = require('../middleware/checkToken');
const offerControllers = require('../controllers/offerController');

// const {
//   // findoffer,
//   // findAll,
//   // updateoffer,
//   // createoffer,
//   // findUseroffers,
//   list, create, update, destroy
// } = require("../controllers/offerController");


router.get('/list', checkToken, offerControllers.list)
router.get('/:id', checkToken, offerControllers.getById)
router.post('/create', checkToken, offerControllers.updateOrCreate);
router.put('/update', checkToken, offerControllers.update);
router.delete('/:id', async function(req, res){
    const id = req.params.id;
    const offerDelete = await offerControllers.remove(id);
    res.send(offerDelete);
})

module.exports = router